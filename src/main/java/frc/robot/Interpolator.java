// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import org.apache.commons.math3.stat.regression.SimpleRegression;

// import frc.robot.Constants;
/** Add your docs here. */
public class Interpolator {
    // public static String function;
    private static SimpleRegression regression;

    // public double[][] dataPoints;
    public Interpolator(String f) {

        regression = new SimpleRegression();
    }

    public static double[][] convertData(double[][] data, String f) {
        double[][] result = new double[data.length][data[0].length];
        if (f.equalsIgnoreCase("log")) {
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[0].length; j++) {
                    result[i][j] = Math.log(data[i][j]);
                }
            }
        } else if (f.equalsIgnoreCase("atan")) {
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[0].length; j++) {
                    result[i][j] = Math.atan(data[i][j]);
                }
            }
        } else if (f.equalsIgnoreCase("linear")) {
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[0].length; j++) {
                    result[i][j] = (data[i][j]);
                }
            }
        }
        return result;
    }

    public static void addDataPoint(String f) {
        if (f.equalsIgnoreCase("log")) { // Logarithm is not defined for x <= 0
            regression.addData(convertData(Constants.PivotInterpolationData.data, "log"));
        } else if (f.equalsIgnoreCase("arctan")) {
            regression.addData(convertData(Constants.PivotInterpolationData.data, "arctan"));
        } else if (f.equalsIgnoreCase("linear")) {
            regression.addData(convertData(Constants.PivotInterpolationData.data, "linear"));
        }
    }

    public static double predictPoint(double x, String f) {
        addDataPoint(f);
        if (f.equalsIgnoreCase("log")) {
            return regression.predict(Math.log(x));
        } else if (f.equalsIgnoreCase("arctan")) {
            return regression.predict(Math.atan(x));
        } else if (f.equalsIgnoreCase("linear")) {
            return regression.predict(x);
        } else if (f.equalsIgnoreCase("sqrt")) {
            return regression.predict(Math.sqrt(x));
        }
        return 0.0;
    }

}
