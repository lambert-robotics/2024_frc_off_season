// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.PS4Controller.Button;
import frc.robot.Constants.AutoConstants;
import frc.robot.Constants.DriveConstants;
import frc.robot.Constants.OIConstants;
import frc.robot.commands.ShootWithoutPivot;
// import frc.robot.commands.ShootWithoutPivot;
// import frc.robot.commands.IntakeNote;
// import frc.robot.commands.MovePivot;
// import frc.robot.commands.Shoot;
import frc.robot.commands.TestCommands.TestIntake;
// import frc.robot.commands.TestCommands.TestPivot;
// import frc.robot.commands.UltimateShooter;
// import frc.robot.commands.TestCommands.TestPivot;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Intake2;
// import frc.robot.subsystems.Pivot;
// import frc.robot.subsystems.Pivot;
import frc.robot.subsystems.Shooterv2;
import frc.robot.subsystems.Vision;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.POVButton;

import java.util.List;

import com.kauailabs.navx.frc.AHRS;

/*
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems
  private final DriveSubsystem m_robotDrive = new DriveSubsystem();
  private final AHRS m_gyro = new AHRS();
  Shooterv2 m_shooter = new Shooterv2();
  Intake m_intake = new Intake();
  // Pivot m_pivot = new Pivot();
  Vision m_vision = new Vision(m_robotDrive);
  // private Vision m_vision = new Vision(m_robotDrive);

  // The driver's controller
  XboxController m_controller = new XboxController(OIConstants.kDriverControllerPort);

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();

    // Configure default commands
    m_robotDrive.setDefaultCommand(
        // The left stick controls translation of the robot.
        // Turning is controlled by the X axis of the right stick.

        new RunCommand(
            () -> m_robotDrive.drive(
                -MathUtil.applyDeadband(m_controller.getLeftY(), OIConstants.kDriveDeadband),
                -MathUtil.applyDeadband(m_controller.getLeftX(), OIConstants.kDriveDeadband),
                -MathUtil.applyDeadband(m_controller.getRightX(), OIConstants.kDriveDeadband),
                true, true),
            m_robotDrive));
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by
   * instantiating a {@link edu.wpi.first.wpilibj.GenericHID} or one of its
   * subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then calling
   * passing it to a
   * {@link JoystickButton}.
   */
  private void configureButtonBindings() {
    new JoystickButton(m_controller, Button.kR1.value)
        .whileTrue(new RunCommand(
            () -> m_robotDrive.setX(),
            m_robotDrive));

    JoystickButton xboxLB = new JoystickButton(m_controller, 5);
    JoystickButton xboxRB = new JoystickButton(m_controller, 6);
    JoystickButton xboxA = new JoystickButton(m_controller, 1);
    JoystickButton xboxB = new JoystickButton(m_controller, 2);
    JoystickButton xboxX = new JoystickButton(m_controller, 3);
    JoystickButton xboxY = new JoystickButton(m_controller, 4);
    JoystickButton xboxMap = new JoystickButton(m_controller, 7);
    JoystickButton xboxStart = new JoystickButton(m_controller, 8);

    // xboxB.whileTrue(new UltimateShooter(1, m_pivot, m_vision, m_intake,
    // m_shooter));

    // xboxY.whileTrue(new TestPivot(m_pivot, 0.1));
    // xboxX.whileTrue(new TestIntake(m_intake, 1));
    // xboxB.whileTrue(new TestIntake(m_intake, -1));
    // xboxA.whileTrue(new TestShooter(m_shooter, m_intake, .8));

    // xboxY.whileTrue(new TestPivot(m_pivot, 0.2));
    // xboxA.whileTrue(new TestPivot(m_pivot, -0.2));

    // xboxA.whileTrue(new IntakeNote(m_intake, 0.1, "intake", m_controller));
    // xboxRB.onTrue(new MovePivot("amp", m_vision, m_pivot));
    // xboxLB.onTrue(new MovePivot("shooter", m_vision, m_pivot));
    // xboxY.onTrue(new MovePivot("shuttle", m_vision, m_pivot));
    // POVButton xboxUp = new POVButton(m_controller, 0);
    // POVButton xboxDown = new POVButton(m_controller, 180);
    // POVButton xboxRight = new POVButton(m_controller, 90);
    // POVButton xboxLeft = new POVButton(m_controller, 270);

    // SCRIW Bindings
    // x --> intake
    // A --> shoot + intake
    // B --> out take

    xboxA.whileTrue(new ShootWithoutPivot(m_shooter, 5900, 50, m_intake));
    xboxX.whileTrue(new TestIntake(m_intake, 1));
    xboxB.whileTrue(new TestIntake(m_intake, -1));

  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // Create config for trajectory
    // TrajectoryConfig config = new TrajectoryConfig(
    //     AutoConstants.kMaxSpeedMetersPerSecond,
    //     AutoConstants.kMaxAccelerationMetersPerSecondSquared)
    //     // Add kinematics to ensure max speed is actually obeyed
    //     .setKinematics(DriveConstants.kDriveKinematics);

    // // An example trajectory to follow. All units in meters.
    // Trajectory exampleTrajectory = TrajectoryGenerator.generateTrajectory(
    //     // Start at the origin facing the +X direction
    //     new Pose2d(0, 0, new Rotation2d(0)),
    //     // Pass through these two interior waypoints, making an 's' curve path
    //     List.of(new Translation2d(1, 1), new Translation2d(2, -1)),
    //     // End 3 meters straight ahead of where we started, facing forward
    //     new Pose2d(3, 0, new Rotation2d(0)),
    //     config);

    // var thetaController = new ProfiledPIDController(
    //     AutoConstants.kPThetaController, 0, 0, AutoConstants.kThetaControllerConstraints);
    // thetaController.enableContinuousInput(-Math.PI, Math.PI);

    // SwerveControllerCommand swerveControllerCommand = new SwerveControllerCommand(
    //     exampleTrajectory,
    //     m_robotDrive::getPose, // Functional interface to feed supplier
    //     DriveConstants.kDriveKinematics,

    //     // Position controllers
    //     new PIDController(AutoConstants.kPXController, 0, 0),
    //     new PIDController(AutoConstants.kPYController, 0, 0),
    //     thetaController,
    //     m_robotDrive::setModuleStates,
    //     m_robotDrive);

    // // Reset odometry to the starting pose of the trajectory.
    // m_robotDrive.resetOdometry(exampleTrajectory.getInitialPose());

    // // Run path following command, then stop at the end.
    // return swerveControllerCommand.andThen(() -> m_robotDrive.drive(0, 0, 0, false, false));
    return new InstantCommand();
  }
}