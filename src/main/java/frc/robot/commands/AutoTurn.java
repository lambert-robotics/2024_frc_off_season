package frc.robot.commands;

import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.Vision;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.math.controller.PIDController;

public class AutoTurn extends Command {

    private PIDController pidController;
    private double Kp = 0.1; // Proportional gain
    private double Ki = 0.0; // Integral gain
    private double Kd = 0.0; // Derivative gain

    private double targetYaw = 0; // Target yaw for alignment
    private double currentYaw; // Current yaw from the IMU
    public DriveSubsystem m_drive;
    public Vision m_vision = new Vision(m_drive);

    /** Creates a new TurnInPlace. */
    public AutoTurn(DriveSubsystem m_drive) {
        this.m_drive = m_drive;
        pidController = new PIDController(Kp, Ki, Kd);
        addRequirements(m_drive);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        currentYaw = m_vision.getYaw();
        pidController.setSetpoint(targetYaw);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        currentYaw = m_vision.getYaw();
        double output = pidController.calculate(currentYaw, 0);

        // Optional: Limit the integral term to prevent windup. Uncomment lines 42 - 45
        // for this feature.

        // double maxOutput = 1.0; // Max output you want to achieve
        // double scaledOutput = output / Math.max(1, Math.abs(output)); // Scale output
        // to -1 to 1
        // Clamp the output to be within -1.0 to 1.0
        // scaledOutput = Math.max(-maxOutput, Math.min(maxOutput, scaledOutput));

        // Turn the robot
        m_drive.drive(0, 0, output, true, true);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        m_drive.drive(0, 0, 0, true, true); // Stop the robot
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        // You can implement a condition to finish the command, for example:
        return Math.abs(currentYaw) < 2; // within 5 degrees
    }
}
