// // Copyright (c) FIRST and other WPILib contributors.
// // Open Source Software; you can modify and/or share it under the terms of
// // the WPILib BSD license file in the root directory of this project.

// package frc.robot.commands;

// import edu.wpi.first.wpilibj.XboxController;
// import edu.wpi.first.wpilibj.GenericHID.RumbleType;
// import edu.wpi.first.wpilibj2.command.Command;
// import frc.robot.Constants;
// import frc.robot.subsystems.Intake;
// // import frc.robot.subsystems.LEDs;

// public class IntakeNote extends Command {
//   /** Creates a new IntakeNoteCommandWithRumble. */
//   private Intake m_intake;
//   private double speed;

//   private double lightVoltage1;
//   private double lightVoltage2;

//   static public boolean presentNote = false;
//   String commandType;
//   // LEDs m_leds;
//   XboxController m_controller;

//   public IntakeNote(Intake intake, double xSpeed, String type, XboxController controller) {
//     m_intake = intake;
//     speed = xSpeed;
//     lightVoltage1 = 4.8;
//     lightVoltage2 = 4.8;
//     commandType = type;
//     // m_leds = leds;
//     m_controller = controller;

//     addRequirements(m_intake);
//     // Use addRequirements() here to declare subsystem dependencies.
//   }

//   // Called when the command is initially scheduled.
//   @Override
//   public void initialize() {
//     lightVoltage1 = m_intake.getSensorOutput(1);
//     lightVoltage2 = m_intake.getSensorOutput(2);

//     m_controller.setRumble(RumbleType.kBothRumble, 0.2);

//     if (lightVoltage1 < Constants.IntakeConstants.intakeSensorLightLimit || lightVoltage2 < Constants.IntakeConstants.intakeSensorLightLimit) {
//       presentNote = true;
//       // m_leds.setOrange();

//     }

//   }

//   // Called every time the scheduler runs while the command is scheduled.
//   @Override
//   public void execute() {
//     // System.out.println(commandType);
//     lightVoltage1 = m_intake.getSensorOutput(1);
//     lightVoltage2 = m_intake.getSensorOutput(2);

//     System.out.println("Light1: " + lightVoltage1);
//     System.out.println("presentNote" + presentNote);

//     if (commandType.equals("intake")) {

//       if (lightVoltage1 < Constants.IntakeConstants.intakeSensorLightLimit || lightVoltage2<Constants.IntakeConstants.intakeSensorLightLimit || presentNote == true) {
//         m_intake.spinIntake(0);
//         presentNote = true;
//         // m_leds.setOrange();
//       } else
//         m_intake.spinIntake(speed);
//     } else if (commandType.equals("shoot")) {

//       m_intake.spinIntake(speed);
//       presentNote = false;
//     }

//   }

//   // Called once the command ends or is interrupted.
//   @Override
//   public void end(boolean interrupted) {
//     m_intake.spinIntake(0);

//     m_controller.setRumble(RumbleType.kBothRumble, 0);

//   }

//   // Returns true when the command should end.
//   @Override
//   public boolean isFinished() {
//     if (lightVoltage1 < Constants.IntakeConstants.intakeSensorLightLimit || lightVoltage2 < Constants.IntakeConstants.intakeSensorLightLimit) {
//       // m_controller.setRumble(RumbleType.kBothRumble, .5);
//       // new WaitCommand(0.5);
//       // m_controller.setRumble(RumbleType.kBothRumble, 0);
//       System.out.println("Done!");
//       return true;
//     }
//     return false;
//   }
// }
