// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;
import frc.robot.Interpolator;
import frc.robot.subsystems.Pivot;
import frc.robot.subsystems.Vision;
import frc.robot.Constants.PivotPositions;
import edu.wpi.first.wpilibj2.command.Command;

public class MovePivot extends Command {
  /** Creates a new MovePivot. */
  Pivot m_pivot;
  String mode;
  double angle;
  double distance;
  Vision m_vision;
  public MovePivot(String mode, Vision vision, Pivot pivot) {
    // Use addRequirements() here to declare subsystem dependencies.
    m_pivot = pivot;
    this.mode = mode;
    m_vision = vision;
    distance = m_vision.distanceToTag();
    if (mode.equalsIgnoreCase("shooter")) {
      distance = m_vision.distanceToTag();
      angle = Interpolator.predictPoint(distance, "log");
    }
    else if (mode.equalsIgnoreCase("Amp")) {
      distance = PivotPositions.ampAngle;
    }
    else if (mode.equalsIgnoreCase("shuttle")) {
      distance = PivotPositions.shuttleAngle;
    }
    addRequirements(m_pivot);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_pivot.stallMotor(0, false);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //5.625 is the conversion value for a gear ratio of 64. For other ratios, conversion factor is (360 / gear ratio);
    
    m_pivot.liftToEncoderValue(angle/5.625);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
