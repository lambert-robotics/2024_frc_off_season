// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Intake2;
import frc.robot.subsystems.Shooterv2;

public class ShootWithoutPivot extends Command {
  private Shooterv2 m_shooter;
  private double speed;
  private double m_deadzone;
  private Intake m_intake;

  // private Timer m_timer;

  /** Creates a new ShootNoteCommand. */
  public ShootWithoutPivot(Shooterv2 shooter, double xSpeed, double deadzone, Intake intake) {
    m_shooter = shooter;
    speed = xSpeed;
    m_deadzone = deadzone;
    m_intake = intake;

    
    addRequirements(m_shooter, m_intake);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_shooter.spinFlyWheels(speed);
   

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println(m_shooter.getSpeed("upper"));
    if (m_shooter.getSpeed("upper") > speed - m_deadzone) {
      System.out.println("AT RIGHT SPEED");
      m_intake.spinIntake(1);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_shooter.spinFlyWheels(0);
    m_intake.stopIntake();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}