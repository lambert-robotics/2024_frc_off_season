// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.TestCommands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Shooterv2;

public class TestShooter extends Command {
  Shooterv2 m_shooter;
  double m_speed;
  Intake m_intake;

  /** Creates a new TestShooter. */
  public TestShooter(Shooterv2 shooter, Intake intake, double speed) {
    m_shooter = shooter;
    m_speed = speed;
    m_intake = intake;
    addRequirements(m_shooter);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_shooter.spinFlyWheels(m_speed);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    m_shooter.spinFlyWheels(m_speed);
    System.out.println("The speed of the upper and lower wheels accordingly are: " + m_shooter.getSpeed("upper")
        + " and " + m_shooter.getSpeed("lower"));

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_shooter.spinFlyWheels(0);

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
