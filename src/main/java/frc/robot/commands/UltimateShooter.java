// // Copyright (c) FIRST and other WPILib contributors.
// // Open Source Software; you can modify and/or share it under the terms of
// // the WPILib BSD license file in the root directory of this project.

// package frc.robot.commands;

// import edu.wpi.first.math.controller.PIDController;
// import edu.wpi.first.wpilibj2.command.Command;
// import frc.robot.subsystems.*;
// import frc.robot.Constants;
// import frc.robot.Constants.UltimateShooterCounters;
// import frc.robot.Interpolator;

// public class UltimateShooter extends Command {
//   Pivot m_pivot;
//   Vision m_vision;
//   Intake m_intake;
//   Shooterv2 m_shooter;
//   double lightVoltage1;
//   double lightVoltage2;
//   private PIDController pidController;
//   private double Kp = 0.1; // Proportional gain
//   private double Ki = 0.0; // Integral gain
//   private double Kd = 0.0; // Derivative gain
//   double angle;
//   double distance;
//   double stage;
//   private double targetYaw = 0; // Target yaw for alignment
//   private double currentYaw; // Current yaw from the IMU
//   public DriveSubsystem m_drive;

//   /** Creates a new UltimateShooter. */
//   public UltimateShooter(int stage, Pivot pivot, Vision vision, Intake intake, Shooterv2 shooter) {
//     // Use addRequirements() here to declare subsystem dependencies.
//     m_pivot = pivot;
//     this.stage = stage;
//     m_vision = vision;
//     m_shooter = shooter;
//     m_intake = intake;
//     pidController = new PIDController(Kp, Ki, Kd);
//     distance = m_vision.distanceToTag();
//     angle = Interpolator.predictPoint(distance, "log");
//     addRequirements(m_shooter);
//   }

//   // Called when the command is initially scheduled.
//   @Override
//   public void initialize() {
//     currentYaw = m_vision.getYaw();
//     if (stage == 1) {
//       pidController.setSetpoint(targetYaw);
//       m_shooter.spinFlyWheels(5900);
//     }

//   }

//   // Called every time the scheduler runs while the command is scheduled.
//   @Override
//   public void execute() {
//     m_pivot.liftToEncoderValue(angle / 5.625);
//     if (stage == 1) {
//       if (!(Math.abs(currentYaw) < 2)) {
//         currentYaw = m_vision.getYaw();
//         double output = pidController.calculate(currentYaw, 0);

//         // Optional: Limit the integral term to prevent windup. Uncomment lines 42 - 45
//         // for this feature.

//         // double maxOutput = 1.0; // Max output you want to achieve
//         // double scaledOutput = output / Math.max(1, Math.abs(output)); // Scale output
//         // to -1 to 1
//         // Clamp the output to be within -1.0 to 1.0
//         // scaledOutput = Math.max(-maxOutput, Math.min(maxOutput, scaledOutput));

//         // Turn the robot
//         m_drive.drive(0, 0, output, true, true);
//       } else {
//         UltimateShooterCounters.counter++;
//       }
//       if (m_pivot.getCurrentAngle() > angle - 2 && m_pivot.getCurrentAngle() < angle + 2) {
//         UltimateShooterCounters.counter++;
//       }

//       if (m_shooter.getSpeed("upper") > 5890) {
//         System.out.println("Shooter is at right speed!");
//         UltimateShooterCounters.counter++;
//       }
//     } else if (stage == 2) {
//       lightVoltage1 = m_intake.getSensorOutput(1);
//       lightVoltage2 = m_intake.getSensorOutput(2);
//       m_intake.spinIntake(0.2);
//     }
//   }

//   @Override
//   public void end(boolean interrupted) {
//     if (!interrupted) {
//       if (stage == 1) {
//         UltimateShooterCounters.counter = 0;
//         new UltimateShooter(2, m_pivot, m_vision, m_intake, m_shooter).schedule(); 
//       } else if (stage == 2) {
//         UltimateShooterCounters.counter = 0;
//         new UltimateShooter(3, m_pivot, m_vision, m_intake, m_shooter).schedule();
//       } else if (stage == 3) {
//         m_shooter.spinFlyWheels(0);
//         m_intake.spinIntake(0);
//         m_pivot.stallMotor(0, false);
//         UltimateShooterCounters.counter = 0;
//         // The next line returns the pivot to its resting point
//         new ZeroPivot(m_pivot).schedule();
//       }
//     } else {
//         m_shooter.spinFlyWheels(0);
//         m_intake.spinIntake(0);
//         m_pivot.stallMotor(0, false);
//         UltimateShooterCounters.counter = 0;
//         // The next line returns the pivot to its resting point
//         new ZeroPivot(m_pivot).schedule();
//         System.out.println("Button was let go before UltimateShooter ended normally");
//     }
//   }

//   // Returns true when the command should end.
//   @Override
//   public boolean isFinished() {
//     if (stage == 1) {
//       if (UltimateShooterCounters.counter == 3) {
//         return true;
//       }
//     } else if (stage == 2) {
//       return (lightVoltage1 < Constants.IntakeConstants.intakeSensorLightLimit)
//           || (lightVoltage2 < Constants.IntakeConstants.intakeSensorLightLimit);
//     } else if (stage == 3) {
//       return true;
//     }
//     return false;
//   }
// }
