// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.Pivot;
import edu.wpi.first.wpilibj2.command.Command;

public class ZeroPivot extends Command {
  /** Creates a new ZeroPivot. */
  Pivot m_pivot;

  public ZeroPivot(Pivot pivot) {
    // Use addRequirements() here to declare subsystem dependencies.
    m_pivot = pivot;
    addRequirements(m_pivot);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (!m_pivot.switchActivated()) {
      m_pivot.spinPivot(-0.1);

    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_pivot.spinPivot(0);
    m_pivot.calibrate();

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return m_pivot.switchActivated();
  }
}
