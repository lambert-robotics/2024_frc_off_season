// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.Constants.IDs;

public class Intake extends SubsystemBase {
  CANSparkMax intakeMotor;

  // AnalogInput sensor;

  // AnalogInput sensor2;

  /** Creates a new IntakeSubsystem. */
  // AnalogInput sensor = new AnalogInput(IDs.intakeSensor1Port);
  // AnalogInput sensor2 = new AnalogInput(IDs.intakeSensor2Port);
  public Intake() {
    intakeMotor = new CANSparkMax(IDs.intakeMotorID, MotorType.kBrushless);

   

  }

  public void spinIntake(double speed) {

    intakeMotor.set(-1*speed);
  }

  public void stopIntake() {

    intakeMotor.set(0);
  }

  // public double getSensorOutput(int oneOrTwo) {
  //   if (oneOrTwo == 1)
  //     return sensor.getVoltage();
  //   else 
  //     return sensor2.getVoltage();
  // }

  // public AnalogInput getSensor(int oneOrTwo) {
  //   if (oneOrTwo == 1)
  //     return sensor;
  //   else 
  //     return sensor2;
  // }

  @Override
  public void periodic() {

  }
}
