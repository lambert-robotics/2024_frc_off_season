// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.controls.Follower;
import com.ctre.phoenix6.controls.VelocityTorqueCurrentFOC;
import com.ctre.phoenix6.hardware.TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.IDs;

public class Intake2 extends SubsystemBase {
  private final TalonFX m_intakeMotor = new TalonFX(IDs.intakeMotorID);

  

  /** Creates a new Intake2. */
  public Intake2() {
    var talonFXConfigs = new TalonFXConfiguration();
    m_intakeMotor.getConfigurator().apply(talonFXConfigs);

  }

  public void spinIntake(double speed) {
    m_intakeMotor.setControl(new DutyCycleOut(speed));
  }

  public void stopIntake() {
    m_intakeMotor.setControl(new DutyCycleOut(0));
  }

  public double getSpeed(String whichWheel) {
    double velocityRotPerSec;
    velocityRotPerSec = m_intakeMotor.getVelocity().getValue();
    // Convert velocity from rotations per second to RPM
    double rpmSpeed = velocityRotPerSec * 60;
    return rpmSpeed;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
