// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;

import javax.sound.midi.Soundbank;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.IDs;

public class Pivot extends SubsystemBase {
    /** Creates a new Pivot. */

    private CANSparkMax pivotMotor1;
    private CANSparkMax pivotMotor2;
    private double voltageLvl;
    private final double kP;
    private final double kI;
    private final double kD;
    DigitalInput limitSwitch1 = new DigitalInput(0);
    DigitalInput limitSwitch2 = new DigitalInput(1);

    private double m_encoderInput;
    private double m_encoderValueError;

    private PIDController m_encoderValuePIDController;

    public Pivot() {
        kP = 2.5;
        kI = 0;
        kD = 0;
        m_encoderValueError = 0;
        m_encoderInput = 0;

        m_encoderValuePIDController = new PIDController(kP,
                kI,
                kD);

        pivotMotor1 = new CANSparkMax(IDs.pivotMotor1ID, MotorType.kBrushless);
        pivotMotor2 = new CANSparkMax(IDs.pivotMotor2ID, MotorType.kBrushless);
        pivotMotor2.setInverted(true);

        // pivotMotor2.follow(pivotMotor1);

        pivotMotor1.setIdleMode(IdleMode.kBrake);
        pivotMotor2.setIdleMode(IdleMode.kCoast);

    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
        m_encoderValueError = pivotMotor1.getEncoder().getPosition();

    }

    public void testPivot(double speed) {

        // if (speed < 0) {
        // pivotMotor1.set(speed);
        // pivotMotor2.set(speed * 1.5);
        // } else {
        // pivotMotor1.set(speed);
        // pivotMotor2.set(speed * 1.5);
        // }
        pivotMotor1.set(speed);
        // pivotMotor2.set(speed);
        System.out.println(pivotMotor2.getEncoder().getVelocity());
    }

    public double getCurrentAngle() {
        return pivotMotor1.getEncoder().getPosition() * 5.625;
    }

    public void liftToEncoderValue(double desiredEncoderValue) {
        if (atDesiredEncoderValue(desiredEncoderValue)) {
            stallMotor(desiredEncoderValue, true);
        } else {
            m_encoderInput = m_encoderValuePIDController.calculate(m_encoderValueError,
                    desiredEncoderValue);

            pivotMotor1.set(m_encoderInput);

        }

    }

    public void spinPivot(double speed) {
        pivotMotor1.set(speed);
    }

    public void calibrate() {
        pivotMotor1.getEncoder().setPosition(0);
        pivotMotor2.getEncoder().setPosition(0);

        System.out.println("The encoder positions are..." + pivotMotor1.getEncoder().getPosition() + " and "
                + pivotMotor2.getEncoder().getPosition());
    }

    public double findDesiredVoltage(double desiredEncoderValue) {
        return Constants.PivotConstants.maxStallVoltage * (Math.sin(desiredEncoderValue) * (180 / Math.PI))
                + Constants.PivotConstants.verticalHoldingVoltage;
    }

    public void stallMotor(double desiredEncoderValue, boolean triggerStalling) {
        if (triggerStalling == true)
            pivotMotor1.setVoltage(desiredEncoderValue);
        else
            pivotMotor1.setVoltage(0);
    }

    public boolean atDesiredEncoderValue(double desiredEncoderValue) {

        if (m_encoderValueError < desiredEncoderValue + 0.05 && m_encoderValueError > desiredEncoderValue - 0.05) {

            System.out.println("The desired encoder position is reached!");

            return true;
        }

        return false;
    }

    public boolean switchActivated() {
        return limitSwitch1.get();
    }
}
