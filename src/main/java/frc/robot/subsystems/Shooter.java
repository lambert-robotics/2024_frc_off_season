package frc.robot.subsystems;

import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.controls.VelocityTorqueCurrentFOC;
import com.ctre.phoenix6.controls.VelocityVoltage;
import com.ctre.phoenix6.controls.VoltageOut;
import com.ctre.phoenix6.hardware.TalonFX;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.IDs;
import com.revrobotics.*;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

// import com.ctre.phoenix6.hardware
public class Shooter extends SubsystemBase {
    private final CANEncoder encoder;
    private final CANPIDController pidController;
    private final TalonFX frontFlyWheel = new TalonFX(IDs.flyWheelHighID);
    private final CANSparkMax shooterSpinner = new CANSparkMax(IDs.shooterSpinnerID, MotorType.kBrushless);
    // private final TalonFX backFlyWheel = new TalonFX(IDs.flyWheelLowID);

    private final VelocityTorqueCurrentFOC m_velocityControl = new VelocityTorqueCurrentFOC(0);
    // private SparkMaxPIDController pidController =
    // shooterSpinner.getPIDController();
    public double kFF, kP, kI, kD, kV, kMaxOutput, kMinOutput, desiredRPM;

    public Shooter() {
        encoder = shooterSpinner.getEncoder();
        pidController = shooterSpinner.getPIDController();
        var talonFXConfigs = new TalonFXConfiguration();
        shooterSpinner.setIdleMode(IdleMode.kBrake);
        // backFlyWheel.getConfigurator().apply(talonFXConfigs);
        frontFlyWheel.getConfigurator().apply(talonFXConfigs);
        // backFlyWheel.setControl(new Follower(frontFlyWheel.getDeviceID(), false));

        kP = 0.1;
        kI = 0;
        kD = 0;
        kV = 0.0;
        kMaxOutput = 1;
        kMinOutput = -1;
        desiredRPM = Constants.ShooterConstants.flyWheelSpeedVelo;

        var slot0Configs = talonFXConfigs.Slot0;
        slot0Configs.kP = kP;
        slot0Configs.kI = kI;
        slot0Configs.kD = kD;
        slot0Configs.kV = kV;
        pidController.setP(kP);
        pidController.setI(kI);
        pidController.setD(kD);
        pidController.setOutputRange(-1.0, 1.0);
        var motorOutputConfigs = talonFXConfigs.MotorOutput;

        motorOutputConfigs.PeakForwardDutyCycle = kMaxOutput;
        motorOutputConfigs.PeakReverseDutyCycle = kMinOutput;

        m_velocityControl.Slot = 0;
        frontFlyWheel.getConfigurator().apply(slot0Configs);
        // backFlyWheel.getConfigurator().apply(slot0Configs);
    }

    public void spinFlyWheels(double speed) {
        final VelocityVoltage m_request = new VelocityVoltage(0).withSlot(0);

        frontFlyWheel.setControl(m_request.withVelocity(100).withFeedForward(11.35).withSlot(0));
    }

    public void spinFlyWheelsNormal(double speed) {
        System.out.println("setting speed to 1");
        frontFlyWheel.setControl(new DutyCycleOut(speed));
    }

    // speed needs to be between 0 and 1. 0 is 0 percent of the max speed, 50 is 50
    // percent of the max speed, and 100 is 100 percent of the max speed;
    public void spinSpinner(double speed) {
        if (Math.abs(speed) > 1) {
            System.out.println("Speed is too high");
            return;
        }
        // pidController.setReference(speed * 120, CANSparkMax.ControlType.kVelocity);

        // if the line above doesnt work, comment it and uncomment the line below.

        shooterSpinner.set(speed);
    }

    public void spinNormal(double speed) {
        if (Math.abs(speed) > 1) {
            System.out.println("Speed is too high");
            return;
        }
        // pidController.setReference(speed * 120, CANSparkMax.ControlType.kVelocity);

        shooterSpinner.set(speed);
        // shooterSpinner.set(speed);
    }

    public double getSpeed(String whichWheel) {
        double velocityRotPerSec = 0;
        if (whichWheel.equals("front")) {
            velocityRotPerSec = frontFlyWheel.getVelocity().getValue();
        } else if (whichWheel.equals("back")) {
            // velocityRotPerSec = backFlyWheel.getVelocity().getValue();
        } else {
            return 0;
        }
        // Convert velocity from rotations per second to RPM
        double rpmSpeed = velocityRotPerSec * 60;
        return rpmSpeed;
    }

    public void setFlyWheelsVoltage(double volts) {
        frontFlyWheel.setControl(new VoltageOut(volts));
    }

    @Override
    public void periodic() {

    }

    public void setAngle(double angle) {
        pidController.setReference(angle, CANSparkMax.ControlType.kPosition);
    }

    public double getCurrentAngle() {
        return encoder.getPosition();
    }

}
