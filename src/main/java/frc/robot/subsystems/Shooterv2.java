package frc.robot.subsystems;

import com.ctre.phoenix6.configs.TalonFXConfiguration;
import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.controls.VelocityTorqueCurrentFOC;
import com.ctre.phoenix6.controls.VoltageOut;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.controls.Follower;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.IDs;

public class Shooterv2 extends SubsystemBase {
    private final TalonFX upperFlyWheel = new TalonFX(IDs.flyWheelHighID);
    private final TalonFX lowerFlywheel = new TalonFX(IDs.flyWheelLowID);

    private final VelocityTorqueCurrentFOC m_velocityControl = new VelocityTorqueCurrentFOC(0);

    public double kP, kI, kD, kV, kMaxOutput, kMinOutput, desiredRPM;

    public Shooterv2() {
        var talonFXConfigs = new TalonFXConfiguration();
        lowerFlywheel.getConfigurator().apply(talonFXConfigs);
        upperFlyWheel.getConfigurator().apply(talonFXConfigs);

        lowerFlywheel.setControl(new Follower(upperFlyWheel.getDeviceID(), false));

        kP = 0.1;
        kI = 0.001;
        kD = 5;
        kV = 0.02;
        kMaxOutput = 1;
        kMinOutput = -1;
        desiredRPM = Constants.ShooterConstants.flyWheelSpeedVelo;

        var slot0Configs = talonFXConfigs.Slot0;
        slot0Configs.kP = kP;
        slot0Configs.kI = kI;
        slot0Configs.kD = kD;
        slot0Configs.kV = kV;

        var motorOutputConfigs = talonFXConfigs.MotorOutput;

        motorOutputConfigs.PeakForwardDutyCycle = kMaxOutput;
        motorOutputConfigs.PeakReverseDutyCycle = kMinOutput;

        upperFlyWheel.getConfigurator().apply(slot0Configs);
        lowerFlywheel.getConfigurator().apply(slot0Configs);
    }

    public void spinFlyWheels(double speed) {
        speed = -speed;
        if (speed != 0) {
            m_velocityControl.Velocity = speed;
            upperFlyWheel.setControl(m_velocityControl);
        } else {
            upperFlyWheel.setControl(new DutyCycleOut(0));
        }
    }

    public void spinFlyWheelsNormal(double speed) {
        upperFlyWheel.setControl(new DutyCycleOut(speed));
    }

    public double getSpeed(String whichWheel) {
        double velocityRotPerSec;
        if (whichWheel.equals("upper")) {
            velocityRotPerSec = upperFlyWheel.getVelocity().getValue();
        } else if (whichWheel.equals("lower")) {
            velocityRotPerSec = lowerFlywheel.getVelocity().getValue();
        } else {
            return 0;
        }
        // Convert velocity from rotations per second to RPM
        double rpmSpeed = velocityRotPerSec * 60;
        return rpmSpeed;
    }
    // change

    public void setFlyWheelsVoltage(double volts) {
        upperFlyWheel.setControl(new VoltageOut(volts));
    }

    @Override
    public void periodic() {

    }
}
