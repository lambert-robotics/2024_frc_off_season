
package frc.robot.subsystems;

import com.revrobotics.CANSparkBase.IdleMode;

import java.util.List;
import java.util.Optional;

import org.photonvision.EstimatedRobotPose;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.PhotonUtils;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;
import org.photonvision.targeting.MultiTargetPNPResult;
import org.photonvision.targeting.PhotonPipelineResult;
import org.photonvision.targeting.PhotonTrackedTarget;
import org.photonvision.targeting.TargetCorner;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
// import frc.robot.Constants.IDs;
import frc.robot.LimelightHelpers;
import frc.robot.LimelightHelpers.LimelightTarget_Barcode;
import frc.robot.Constants;

public class Vision extends SubsystemBase {
    /** Creates a new AmpSubsystem. */
    PhotonCamera m_camera = new PhotonCamera("9477");
    // PhotonCamera m_camera1 = new PhotonCamera("cam1");
    // PhotonCamera m_camera2 = new PhotonCamera("cam2");
    // PhotonCamera m_camera3 = new PhotonCamera("cam3");
    // PhotonCamera m_camera4 = new PhotonCamera("cam4");

    AprilTagFieldLayout aprilTagFieldLayout = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField();
    private Pose2d estimatedPose;
    private SwerveDrivePoseEstimator poseEstimator;

    private Pose2d m_limelightPose2d;
    private String limelightName = "limelightCam1";
    private NetworkTableInstance limelighNetworkTable;

    private PhotonPoseEstimator camera1PoseEstimator;
    private PhotonPoseEstimator camera2PoseEstimator;
    private PhotonPoseEstimator camera3PoseEstimator;
    private PhotonPoseEstimator camera4PoseEstimator;

    private Pose2d prevEstPose1 = null;
    private Pose2d prevEstPose2 = null;
    private Pose2d prevEstPose3 = null;
    private Pose2d prevEstPose4 = null;

    Boolean limelight1detection;

    Boolean cam1detection;
    Boolean cam2detection;
    Boolean cam3detection;
    Boolean cam4detection;

    Transform3d m_camera1_location;
    Transform3d m_camera2_location;
    Transform3d m_camera3_location;
    Transform3d m_camera4_location;

    private PhotonPipelineResult m_result1;
    private List<PhotonTrackedTarget> m_targets1;
    private PhotonPipelineResult m_result2;
    private List<PhotonTrackedTarget> m_targets2;
    private PhotonPipelineResult m_result3;
    private List<PhotonTrackedTarget> m_targets3;
    private PhotonPipelineResult m_result4;
    private List<PhotonTrackedTarget> m_targets4;

    private PhotonPipelineResult m_result;
    private List<PhotonTrackedTarget> m_targets;
    private PhotonTrackedTarget m_desiredTarget;
    private double m_yaw;
    private double m_xyRatio;
    // positive is right
    private double m_areaOfTarget;
    private DriveSubsystem m_drive;
    // should get smaller as you get further, vice versa
    public final double m_DESIRED_AREA_OF_TARGET = .5;
    public final double m_CAMERA_HEIGHT = 0.54;
    public final double m_TARGET_HEIGHT = 1.38;
    public final double m_CAMERA_PITCH = 15.0;
    private ShuffleboardTab visionTab;

    private final double TAG_WIDTH = 0.1524; // Example tag width in meters (6 inches)

    public Vision(DriveSubsystem drive_copy) {
        m_result1 = null;
        m_targets1 = null;
        m_result2 = null;
        m_targets2 = null;
        m_result3 = null;
        m_targets3 = null;
        m_result4 = null;
        m_targets4 = null;
        m_desiredTarget = null;
        m_yaw = 0;
        m_areaOfTarget = 0;
        m_xyRatio = 0;
        m_drive = drive_copy;
        limelight1detection = false;

        // camera1PoseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout,
        // PoseStrategy.MULTI_TAG_PNP_ON_COPROCESSOR,
        // m_camera1, m_camera1_location);

        // double limelighNetworkTableInstance =
        // NetworkTableInstance.getDefault().getTable("limelight").getEntry("<variablename>").getDouble(0);

        // camera2PoseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout,
        // PoseStrategy.MULTI_TAG_PNP_ON_COPROCESSOR,
        // m_camera2, m_camera2_location);
        // camera3PoseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout,
        // PoseStrategy.MULTI_TAG_PNP_ON_COPROCESSOR,
        // m_camera3, m_camera3_location);
        // camera4PoseEstimator = new PhotonPoseEstimator(aprilTagFieldLayout,
        // PoseStrategy.MULTI_TAG_PNP_ON_COPROCESSOR,
        // m_camera4, m_camera4_location);

        visionTab = Shuffleboard.getTab("Vision");

    }

    public void updateTargetValues() {
        m_areaOfTarget = m_desiredTarget.getArea();
        m_yaw = m_desiredTarget.getYaw();
        m_xyRatio = getXYRatio();

    }

    public boolean setDesiredTarget(int id, int id2) {
        m_result = m_camera.getLatestResult();
        m_targets = m_result.getTargets();
        System.out.println("These are the IDs being detected by the camera...");
        for (PhotonTrackedTarget target : m_targets) {
            System.out.println(target.getFiducialId());
            if (target.getFiducialId() == id || target.getFiducialId() == id2) {
                m_desiredTarget = target;
                System.out.println("The desired target with the id of " +
                        m_desiredTarget.getFiducialId()
                        + " has been detected.");
                return true;
            }
        }
        return false;
    }

    public double getYaw() {
        m_result = m_camera.getLatestResult();
        m_targets = m_result.getTargets();
        for (PhotonTrackedTarget target : m_targets) {
            if (target.getFiducialId() == 4 || target.getFiducialId() == 7) {
                System.out.println("Target Yaw: " + target.getYaw());
                return target.getYaw();
            }
        }
        System.out.println("Valid IDs not found");
        return 100000;
    }

    public boolean isInAreaRange() {
        double lowerBound = m_DESIRED_AREA_OF_TARGET - 0.02;
        double upperBound = m_DESIRED_AREA_OF_TARGET + 0.02;
        if (m_areaOfTarget > lowerBound && m_areaOfTarget < upperBound) {
            System.out.println("The desired target is in the Area range!");
            return true;
        }
        return false;
    }

    public boolean isInYawRange() {
        if (m_yaw > -6 && m_yaw < -2) {
            System.out.println("The desired target is in the Yaw range!");
            return true;
        }
        return false;
    }

    public boolean desiredTargetDetected() {
        return m_desiredTarget != null;
    }

    private double getXYRatio() {
        List<TargetCorner> corners = m_desiredTarget.getDetectedCorners();
        double xDistance = corners.get(1).x - corners.get(0).x;
        double yDistance = corners.get(3).y - corners.get(0).y;
        double xyRatio = xDistance / yDistance;
        System.out.println("The X/Y ratio is: " + xyRatio);
        return xyRatio;
    }

    private void prints() {
        System.out.println("The desired target was successfully set!");
        System.out.println("A desired target was recognized...");
        System.out.println("The target ID is: " + m_desiredTarget.getFiducialId());
        System.out.println("The desired target's AREA on the camera is: " + m_areaOfTarget);
        System.out.println("The desired target's YAW is: " + m_yaw);
        System.out.println("The distance to the target in meters: " + PhotonUtils.calculateDistanceToTargetMeters(
                m_CAMERA_HEIGHT, m_TARGET_HEIGHT, m_CAMERA_PITCH, m_desiredTarget.getPitch()));
    }

    private void getCameraToBot() {

        Rotation3d m_camera1_rotation = new Rotation3d(0, -20, 180);

        // Rotation3d m_camera2_rotation = new Rotation3d(0, 20, -45);

        // Rotation3d m_camera3_rotation = new Rotation3d(0, 20, 135);

        // Rotation3d m_camera4_rotation = new Rotation3d(0, 20, 135);

        // position of cameras relative to bots center(inches)
        m_camera1_location = new Transform3d(0, 12.375, 0, m_camera1_rotation);
        // m_camera2_location = new Transform3d(-12.375, 12.375, 0, m_camera2_rotation);
        // m_camera3_location = new Transform3d(12.375, -12.375, 0, m_camera3_rotation);
        // m_camera4_location = new Transform3d(-12.375, -12.375, 0,
        // m_camera4_rotation);

    }

    public double distanceToTag() {
        m_result = m_camera.getLatestResult();
        m_targets = m_result.getTargets();
        Pose2d position = poseEstimator.getEstimatedPosition();
        double x = position.getX();
        double y = position.getY();
        double distance = -1.0D;
        for (PhotonTrackedTarget tag : m_targets) {
            if (tag.getFiducialId() == 4 || tag.getFiducialId() == 7) {
                if (tag.getFiducialId() == 4) {
                    distance = Math.sqrt(Math.pow(652.73 - x, 2) + Math.pow(218.42 - y, 2));
                } else {
                    distance = Math.sqrt(Math.pow(-1.5 - x, 2) + Math.pow(218.42 - y, 2));
                }
            }
        }
        if (distance == -1) {
            System.out.println("THE CORRECT TAGS AREN'T IN VIEW");
        }
        return distance;
    }
    public Pose2d getRobotPose() {
        Pose2d position = poseEstimator.getEstimatedPosition();
        return position;
    }
    public void updateTargetDetection() {
        // for camera 1
        // m_result1 = m_camera1.getLatestResult();
        // m_targets1 = m_result1.getTargets();
        // if (m_targets1.size() == 0)
        // cam1detection = true;
        // else
        // cam1detection = false;

        // if(limelighNetworkTable.getDefault().getTable("limelight").getEntry("tv").getDouble(0)==1){
        // limelight1detection = true;
        // } else
        // if(limelighNetworkTable.getDefault().getTable("limelight").getEntry("tv").getDouble(0)==0)
        // limelight1detection = false;
        // else{
        // limelight1detection = false;
        // }
        // // for camera 2
        // m_result2 = m_camera2.getLatestResult();
        // m_targets2 = m_result2.getTargets();
        // if (m_targets2.size() == 0)
        // cam2detection = true;
        // else
        // cam2detection = false;
        // // for camera 3
        // m_result3 = m_camera3.getLatestResult();
        // m_targets3 = m_result3.getTargets();
        // if (m_targets3.size() == 0)
        // cam3detection = true;
        // else
        // cam3detection = false;
        // // for camera 4
        // m_result4 = m_camera4.getLatestResult();
        // m_targets4 = m_result4.getTargets();
        // if (m_targets4.size() == 0)
        // cam4detection = true;
        // else
        // cam4detection = false;

    }

    @Override
    public void periodic() {
        if (desiredTargetDetected()) {
            updateTargetValues();
            prints();
            visionTab.addCamera("Camera", "9477", "url");
            visionTab.addDouble("Area", () -> m_areaOfTarget);
            visionTab.addDouble("Yaw", () -> m_yaw);
            visionTab.addDouble("XY-Ratio", () -> m_xyRatio);
            visionTab.addDouble("Desired Target ID", () -> m_desiredTarget.getFiducialId());
            visionTab.addBoolean("Target Detected", () -> desiredTargetDetected());
        }
        updateTargetDetection();
        // if there is an error from these, I am guessing it is because the cameras
        // aren't attached to the bot -Yash
        // if(m_camera1.isConnected()){
        // if (cam1detection) {
        // if (prevEstPose1 != null)
        // camera1PoseEstimator.setReferencePose(prevEstPose1);

        // Optional<EstimatedRobotPose> update1 =
        // camera1PoseEstimator.update(m_result1);
        // Pose2d cam1Pose2d = update1.get().estimatedPose.toPose2d();
        // double timestamp1 = update1.get().timestampSeconds;
        // prevEstPose1 = cam1Pose2d;
        // m_drive.useCameraPose(cam1Pose2d, timestamp1);

        // }
        // }

        // if(limelight1detection){
        // LimelightHelpers.PoseEstimate limelightMeasurement =
        // LimelightHelpers.getBotPoseEstimate_wpiBlue("limelight");
        // m_drive.useCameraPose(LimelightHelpers.getBotPose2d(limelightName),
        // limelightMeasurement.timestampSeconds);
        // }
        // if (cam2detection) {
        // if (prevEstPose2 != null)
        // camera2PoseEstimator.setReferencePose(prevEstPose2);

        // Optional<EstimatedRobotPose> update2 =
        // camera2PoseEstimator.update(m_result2);
        // Pose2d cam2Pose2d = update2.get().estimatedPose.toPose2d();
        // double timestamp2 = update2.get().timestampSeconds;
        // prevEstPose2 = cam2Pose2d;
        // m_drive.useCameraPose(cam2Pose2d, timestamp2);

        // }
        // if (cam3detection) {
        // if (prevEstPose3 != null)
        // camera3PoseEstimator.setReferencePose(prevEstPose3);

        // Optional<EstimatedRobotPose> update3 =
        // camera3PoseEstimator.update(m_result3);
        // Pose2d cam3Pose2d = update3.get().estimatedPose.toPose2d();
        // double timestamp3 = update3.get().timestampSeconds;
        // prevEstPose3 = cam3Pose2d;
        // m_drive.useCameraPose(cam3Pose2d, timestamp3);

        // }
        // if (cam4detection) {
        // if (prevEstPose4 != null)
        // camera4PoseEstimator.setReferencePose(prevEstPose4);
        // Optional<EstimatedRobotPose> update4 =
        // camera4PoseEstimator.update(m_result4);
        // Pose2d cam4Pose2d = update4.get().estimatedPose.toPose2d();
        // double timestamp4 = update4.get().timestampSeconds;
        // prevEstPose4 = cam4Pose2d;
        // m_drive.useCameraPose(cam4Pose2d, timestamp4);

        // }

    }

    private double calculateZCenter(PhotonTrackedTarget tag) {
        // Assuming you have the focal length of your camera in pixels
        double focalLength = 600; // Example focal length in pixels

        // Width of the tag in pixels in the image (use tag.getBoundingBox() or similar
        // if available)
        double widthInPixels = tag.getDetectedCorners().get(1).x - tag.getDetectedCorners().get(0).x;// Example method
                                                                                                     // // box width

        // Z = (TAG_WIDTH * FOCAL_LENGTH) / WIDTH_IN_PIXELS
        return (TAG_WIDTH * focalLength) / widthInPixels;
    }

    // public PhotonCamera getCamera() {
    // return m_camera;
    // }

}
